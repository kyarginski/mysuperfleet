﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySuperFleet
{
    public partial class frmVehicle_RepairEdit : frmBaseEdit
    {
        public frmVehicle_RepairEdit()
        {
            InitializeComponent();
        }

        protected override bool doValidate()
        {
            bool ret = true;

            if ((comboBoxParent.Text.Length == 0) &&
                    (comboBoxRepair.Text.Length == 0) &&
                    (comboBoxInspection.Text.Length == 0)) {
                MessageBox.Show("One of the values of "+ Environment.NewLine+ Environment.NewLine +
                    labelRepair.Text + Environment.NewLine +
                    labelParent.Text + Environment.NewLine +
                    labelInspection.Text + Environment.NewLine +
                    Environment.NewLine +
                    "have to be filled!", 
                    "Attention!", 
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                ret = false;
            }

            return ret;
        }

    }
}

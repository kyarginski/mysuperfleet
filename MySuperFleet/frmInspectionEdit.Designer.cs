﻿namespace MySuperFleet
{
    partial class frmInspectionEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.edNote = new System.Windows.Forms.TextBox();
            this.edIns_Date = new System.Windows.Forms.DateTimePicker();
            this.labelIns_Date = new System.Windows.Forms.Label();
            this.edIns_Num = new System.Windows.Forms.TextBox();
            this.labelIns_Num = new System.Windows.Forms.Label();
            this.edType = new System.Windows.Forms.TextBox();
            this.labelType = new System.Windows.Forms.Label();
            this.panelButtom.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelButtom
            // 
            this.panelButtom.Size = new System.Drawing.Size(465, 207);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(384, 180);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(303, 180);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.groupBox1);
            this.panelMain.Controls.Add(this.edIns_Date);
            this.panelMain.Controls.Add(this.labelIns_Date);
            this.panelMain.Controls.Add(this.edIns_Num);
            this.panelMain.Controls.Add(this.labelIns_Num);
            this.panelMain.Controls.Add(this.edType);
            this.panelMain.Controls.Add(this.labelType);
            this.panelMain.Size = new System.Drawing.Size(465, 285);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.edNote);
            this.groupBox1.Location = new System.Drawing.Point(28, 114);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 148);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Note";
            // 
            // edNote
            // 
            this.edNote.Location = new System.Drawing.Point(6, 19);
            this.edNote.Multiline = true;
            this.edNote.Name = "edNote";
            this.edNote.Size = new System.Drawing.Size(402, 123);
            this.edNote.TabIndex = 0;
            // 
            // edIns_Date
            // 
            this.edIns_Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.edIns_Date.Location = new System.Drawing.Point(302, 85);
            this.edIns_Date.Name = "edIns_Date";
            this.edIns_Date.Size = new System.Drawing.Size(140, 20);
            this.edIns_Date.TabIndex = 14;
            // 
            // labelIns_Date
            // 
            this.labelIns_Date.AutoSize = true;
            this.labelIns_Date.Location = new System.Drawing.Point(31, 91);
            this.labelIns_Date.Name = "labelIns_Date";
            this.labelIns_Date.Size = new System.Drawing.Size(82, 13);
            this.labelIns_Date.TabIndex = 13;
            this.labelIns_Date.Text = "Inspection Date";
            // 
            // edIns_Num
            // 
            this.edIns_Num.Location = new System.Drawing.Point(302, 48);
            this.edIns_Num.Name = "edIns_Num";
            this.edIns_Num.Size = new System.Drawing.Size(140, 20);
            this.edIns_Num.TabIndex = 12;
            // 
            // labelIns_Num
            // 
            this.labelIns_Num.AutoSize = true;
            this.labelIns_Num.Location = new System.Drawing.Point(31, 51);
            this.labelIns_Num.Name = "labelIns_Num";
            this.labelIns_Num.Size = new System.Drawing.Size(96, 13);
            this.labelIns_Num.TabIndex = 11;
            this.labelIns_Num.Text = "Inspection Number";
            // 
            // edType
            // 
            this.edType.Location = new System.Drawing.Point(302, 12);
            this.edType.Name = "edType";
            this.edType.Size = new System.Drawing.Size(140, 20);
            this.edType.TabIndex = 10;
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(31, 15);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(83, 13);
            this.labelType.TabIndex = 9;
            this.labelType.Text = "Inspection Type";
            // 
            // frmInspectionEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 339);
            this.Name = "frmInspectionEdit";
            this.Text = "Inspection";
            this.panelButtom.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox edNote;
        public System.Windows.Forms.DateTimePicker edIns_Date;
        public System.Windows.Forms.Label labelIns_Date;
        public System.Windows.Forms.TextBox edIns_Num;
        public System.Windows.Forms.Label labelIns_Num;
        public System.Windows.Forms.TextBox edType;
        public System.Windows.Forms.Label labelType;
    }
}
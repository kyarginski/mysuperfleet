﻿namespace MySuperFleet
{
    partial class frmVehicleEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelType = new System.Windows.Forms.Label();
            this.edType = new System.Windows.Forms.TextBox();
            this.edModel = new System.Windows.Forms.TextBox();
            this.labelModel = new System.Windows.Forms.Label();
            this.edReg_Num = new System.Windows.Forms.TextBox();
            this.labelReg_Num = new System.Windows.Forms.Label();
            this.labelReg_Date = new System.Windows.Forms.Label();
            this.edReg_Date = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.edNote = new System.Windows.Forms.TextBox();
            this.comboBoxFleet = new System.Windows.Forms.ComboBox();
            this.labelFleet = new System.Windows.Forms.Label();
            this.panelButtom.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelButtom
            // 
            this.panelButtom.Size = new System.Drawing.Size(484, 236);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(403, 209);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(322, 209);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.labelFleet);
            this.panelMain.Controls.Add(this.comboBoxFleet);
            this.panelMain.Controls.Add(this.groupBox1);
            this.panelMain.Controls.Add(this.edReg_Date);
            this.panelMain.Controls.Add(this.labelReg_Date);
            this.panelMain.Controls.Add(this.edReg_Num);
            this.panelMain.Controls.Add(this.labelReg_Num);
            this.panelMain.Controls.Add(this.edModel);
            this.panelMain.Controls.Add(this.labelModel);
            this.panelMain.Controls.Add(this.edType);
            this.panelMain.Controls.Add(this.labelType);
            this.panelMain.Size = new System.Drawing.Size(484, 314);
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(30, 29);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(31, 13);
            this.labelType.TabIndex = 0;
            this.labelType.Text = "Type";
            // 
            // edType
            // 
            this.edType.Location = new System.Drawing.Point(103, 26);
            this.edType.Name = "edType";
            this.edType.Size = new System.Drawing.Size(140, 20);
            this.edType.TabIndex = 1;
            // 
            // edModel
            // 
            this.edModel.Location = new System.Drawing.Point(307, 26);
            this.edModel.Name = "edModel";
            this.edModel.Size = new System.Drawing.Size(140, 20);
            this.edModel.TabIndex = 3;
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Location = new System.Drawing.Point(249, 29);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(36, 13);
            this.labelModel.TabIndex = 2;
            this.labelModel.Text = "Model";
            // 
            // edReg_Num
            // 
            this.edReg_Num.Location = new System.Drawing.Point(103, 64);
            this.edReg_Num.Name = "edReg_Num";
            this.edReg_Num.Size = new System.Drawing.Size(140, 20);
            this.edReg_Num.TabIndex = 5;
            // 
            // labelReg_Num
            // 
            this.labelReg_Num.AutoSize = true;
            this.labelReg_Num.Location = new System.Drawing.Point(30, 67);
            this.labelReg_Num.Name = "labelReg_Num";
            this.labelReg_Num.Size = new System.Drawing.Size(67, 13);
            this.labelReg_Num.TabIndex = 4;
            this.labelReg_Num.Text = "Reg Number";
            // 
            // labelReg_Date
            // 
            this.labelReg_Date.AutoSize = true;
            this.labelReg_Date.Location = new System.Drawing.Point(249, 67);
            this.labelReg_Date.Name = "labelReg_Date";
            this.labelReg_Date.Size = new System.Drawing.Size(53, 13);
            this.labelReg_Date.TabIndex = 6;
            this.labelReg_Date.Text = "Reg Date";
            // 
            // edReg_Date
            // 
            this.edReg_Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.edReg_Date.Location = new System.Drawing.Point(307, 64);
            this.edReg_Date.Name = "edReg_Date";
            this.edReg_Date.Size = new System.Drawing.Size(140, 20);
            this.edReg_Date.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.edNote);
            this.groupBox1.Location = new System.Drawing.Point(33, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 148);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Note";
            // 
            // edNote
            // 
            this.edNote.Location = new System.Drawing.Point(6, 19);
            this.edNote.Multiline = true;
            this.edNote.Name = "edNote";
            this.edNote.Size = new System.Drawing.Size(402, 123);
            this.edNote.TabIndex = 0;
            // 
            // comboBoxFleet
            // 
            this.comboBoxFleet.FormattingEnabled = true;
            this.comboBoxFleet.Location = new System.Drawing.Point(103, 108);
            this.comboBoxFleet.Name = "comboBoxFleet";
            this.comboBoxFleet.Size = new System.Drawing.Size(344, 21);
            this.comboBoxFleet.TabIndex = 9;
            // 
            // labelFleet
            // 
            this.labelFleet.AutoSize = true;
            this.labelFleet.Location = new System.Drawing.Point(30, 111);
            this.labelFleet.Name = "labelFleet";
            this.labelFleet.Size = new System.Drawing.Size(30, 13);
            this.labelFleet.TabIndex = 10;
            this.labelFleet.Text = "Fleet";
            // 
            // frmVehicleEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 368);
            this.Name = "frmVehicleEdit";
            this.Text = "Vehicle";
            this.panelButtom.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Label labelReg_Date;
        public System.Windows.Forms.TextBox edReg_Num;
        public System.Windows.Forms.Label labelReg_Num;
        public System.Windows.Forms.TextBox edModel;
        public System.Windows.Forms.Label labelModel;
        public System.Windows.Forms.TextBox edType;
        public System.Windows.Forms.Label labelType;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.DateTimePicker edReg_Date;
        public System.Windows.Forms.TextBox edNote;
        public System.Windows.Forms.Label labelFleet;
        public System.Windows.Forms.ComboBox comboBoxFleet;
    }
}
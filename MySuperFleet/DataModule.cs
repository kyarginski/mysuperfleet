﻿using System.Data;
using System.Data.SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySuperFleet
{

    class DataModule
    {
        public SQLiteConnection connection;


        public DataModule()
        {
            connection = new SQLiteConnection("Data Source=datafile.sqlite;Version=3;New=False;Compress=True;");
        }

        ~DataModule()
        {
            if (connection != null)
            {
                connection.Dispose();
            }
        }

        public int executeQuery(string txtQuery)
        {
            int result;
            SQLiteCommand sql_cmd;
            try
            {
                connection.Open();
                sql_cmd = connection.CreateCommand();
                sql_cmd.CommandText = txtQuery;
                result = sql_cmd.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public DataTable openQuery(string txtQuery)
        {
            SQLiteDataAdapter DB;
            SQLiteCommand sql_cmd;
            DataSet DS = new DataSet();
            DataTable DT = new DataTable();
            try
            {
                connection.Open();
                sql_cmd = connection.CreateCommand();
                sql_cmd.CommandText = txtQuery;

                DB = new SQLiteDataAdapter(txtQuery, connection);
                DS.Reset();
                DB.Fill(DS);
                DT = DS.Tables[0];
            }
            finally
            {
                connection.Close();
            }
            return DT;
        }

        public int getLastInsertedId(string tableName)
        {
            int result = 0;
            DataTable DT = new DataTable();

            DT = openQuery("SELECT SEQ from sqlite_sequence WHERE name='"+ tableName + "'");

            foreach (DataRow row in DT.Rows)
            {
                int.TryParse(row.ItemArray[0].ToString(), out result);
                break;
            }

            //result = DT.Rows[0].Field<int>(0);

            return result;
        }

        public void WriteToCsvFile(DataTable dataTable, string filePath, String separator = ",")
        {
            StringBuilder fileContent = new StringBuilder();

            foreach (var col in dataTable.Columns)
            {
                fileContent.Append(col.ToString() + separator);
            }

            fileContent.Replace(separator, System.Environment.NewLine, fileContent.Length - 1, 1);

            foreach (DataRow dr in dataTable.Rows)
            {

                foreach (var column in dr.ItemArray)
                {
                    fileContent.Append("\"" + column.ToString() + "\"" + separator);
                }

                fileContent.Replace(separator, System.Environment.NewLine, fileContent.Length - 1, 1);
            }

            System.IO.File.WriteAllText(filePath, fileContent.ToString());

        }

    }
}

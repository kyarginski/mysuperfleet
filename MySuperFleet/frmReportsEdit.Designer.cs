﻿namespace MySuperFleet
{
    partial class frmReportsEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.edSQL_Text = new System.Windows.Forms.TextBox();
            this.edReport_Name = new System.Windows.Forms.TextBox();
            this.labelReport_Name = new System.Windows.Forms.Label();
            this.labelReport_Type = new System.Windows.Forms.Label();
            this.comboReport_Type = new System.Windows.Forms.ComboBox();
            this.panelButtom.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelButtom
            // 
            this.panelButtom.Size = new System.Drawing.Size(458, 216);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(377, 189);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(296, 189);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.labelReport_Type);
            this.panelMain.Controls.Add(this.comboReport_Type);
            this.panelMain.Controls.Add(this.groupBox1);
            this.panelMain.Controls.Add(this.edReport_Name);
            this.panelMain.Controls.Add(this.labelReport_Name);
            this.panelMain.Size = new System.Drawing.Size(458, 294);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.edSQL_Text);
            this.groupBox1.Location = new System.Drawing.Point(11, 84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(425, 198);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SQL text";
            // 
            // edSQL_Text
            // 
            this.edSQL_Text.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edSQL_Text.Location = new System.Drawing.Point(6, 19);
            this.edSQL_Text.Multiline = true;
            this.edSQL_Text.Name = "edSQL_Text";
            this.edSQL_Text.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edSQL_Text.Size = new System.Drawing.Size(413, 173);
            this.edSQL_Text.TabIndex = 0;
            // 
            // edReport_Name
            // 
            this.edReport_Name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edReport_Name.Location = new System.Drawing.Point(123, 18);
            this.edReport_Name.Name = "edReport_Name";
            this.edReport_Name.Size = new System.Drawing.Size(313, 20);
            this.edReport_Name.TabIndex = 22;
            // 
            // labelReport_Name
            // 
            this.labelReport_Name.AutoSize = true;
            this.labelReport_Name.Location = new System.Drawing.Point(14, 21);
            this.labelReport_Name.Name = "labelReport_Name";
            this.labelReport_Name.Size = new System.Drawing.Size(70, 13);
            this.labelReport_Name.TabIndex = 21;
            this.labelReport_Name.Text = "Report Name";
            // 
            // labelReport_Type
            // 
            this.labelReport_Type.AutoSize = true;
            this.labelReport_Type.Location = new System.Drawing.Point(13, 52);
            this.labelReport_Type.Name = "labelReport_Type";
            this.labelReport_Type.Size = new System.Drawing.Size(66, 13);
            this.labelReport_Type.TabIndex = 25;
            this.labelReport_Type.Text = "Report Type";
            // 
            // comboReport_Type
            // 
            this.comboReport_Type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboReport_Type.FormattingEnabled = true;
            this.comboReport_Type.Items.AddRange(new object[] {
            "CSV"});
            this.comboReport_Type.Location = new System.Drawing.Point(346, 49);
            this.comboReport_Type.Name = "comboReport_Type";
            this.comboReport_Type.Size = new System.Drawing.Size(89, 21);
            this.comboReport_Type.TabIndex = 24;
            // 
            // frmReportsEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 348);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "frmReportsEdit";
            this.Text = "Report";
            this.panelButtom.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox edSQL_Text;
        public System.Windows.Forms.TextBox edReport_Name;
        public System.Windows.Forms.Label labelReport_Name;
        public System.Windows.Forms.Label labelReport_Type;
        public System.Windows.Forms.ComboBox comboReport_Type;
    }
}
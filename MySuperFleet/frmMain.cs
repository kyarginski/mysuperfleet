﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace MySuperFleet
{
	public class frmMain : Form
	{
        DataModule dm;

        private System.Windows.Forms.MainMenu mainMenu1;

		int count=0;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItemAcs;
		private System.Windows.Forms.MenuItem menuItemHoriz;
		private System.Windows.Forms.MenuItem menuItemVert;
		private System.Windows.Forms.MenuItem menuItemAI;
		private System.Windows.Forms.MenuItem menuItemFile;
		private System.Windows.Forms.MenuItem menuItemClose;
		private System.Windows.Forms.MenuItem menuItemWindow;
		private System.Windows.Forms.MenuItem menuItemMax;
		private System.Windows.Forms.MenuItem menuItemMin;
		private System.Windows.Forms.OpenFileDialog oFileDlg;
		private System.Windows.Forms.MenuItem menuItem1;

		public frmMain()
		{

			InitializeComponent();

            dm = new DataModule();

            menuItemVehiclesAndRepairs_Click(null, null);
        }


        protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItemFile = new System.Windows.Forms.MenuItem();
            this.menuItemVehiclesAndRepairs = new System.Windows.Forms.MenuItem();
            this.menuItemReports = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItemFleet = new System.Windows.Forms.MenuItem();
            this.menuItemVehicles = new System.Windows.Forms.MenuItem();
            this.menuItemInspections = new System.Windows.Forms.MenuItem();
            this.menuItemRepairs = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItemClose = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItemExit = new System.Windows.Forms.MenuItem();
            this.menuItemWindow = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItemAI = new System.Windows.Forms.MenuItem();
            this.menuItemAcs = new System.Windows.Forms.MenuItem();
            this.menuItemHoriz = new System.Windows.Forms.MenuItem();
            this.menuItemVert = new System.Windows.Forms.MenuItem();
            this.menuItemMax = new System.Windows.Forms.MenuItem();
            this.menuItemMin = new System.Windows.Forms.MenuItem();
            this.oFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFile,
            this.menuItemWindow,
            this.menuItem5});
            // 
            // menuItemFile
            // 
            this.menuItemFile.Index = 0;
            this.menuItemFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemVehiclesAndRepairs,
            this.menuItemReports,
            this.menuItem4,
            this.menuItemFleet,
            this.menuItemVehicles,
            this.menuItemInspections,
            this.menuItemRepairs,
            this.menuItem2,
            this.menuItem1,
            this.menuItemClose,
            this.menuItem3,
            this.menuItemExit});
            this.menuItemFile.MergeType = System.Windows.Forms.MenuMerge.MergeItems;
            this.menuItemFile.Text = "&File";
            // 
            // menuItemVehiclesAndRepairs
            // 
            this.menuItemVehiclesAndRepairs.Index = 0;
            this.menuItemVehiclesAndRepairs.Text = "Vehicles && Repairs...";
            this.menuItemVehiclesAndRepairs.Click += new System.EventHandler(this.menuItemVehiclesAndRepairs_Click);
            // 
            // menuItemReports
            // 
            this.menuItemReports.Index = 1;
            this.menuItemReports.Text = "Reports...";
            this.menuItemReports.Click += new System.EventHandler(this.menuItemReports_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 2;
            this.menuItem4.Text = "-";
            // 
            // menuItemFleet
            // 
            this.menuItemFleet.Index = 3;
            this.menuItemFleet.Text = "Fleet view...";
            this.menuItemFleet.Click += new System.EventHandler(this.menuItemFleet_Click);
            // 
            // menuItemVehicles
            // 
            this.menuItemVehicles.Index = 4;
            this.menuItemVehicles.Text = "Vehicles view...";
            this.menuItemVehicles.Click += new System.EventHandler(this.menuItemVehicles_Click);
            // 
            // menuItemInspections
            // 
            this.menuItemInspections.Index = 5;
            this.menuItemInspections.Text = "Inspections view...";
            this.menuItemInspections.Click += new System.EventHandler(this.menuItemInspections_Click);
            // 
            // menuItemRepairs
            // 
            this.menuItemRepairs.Index = 6;
            this.menuItemRepairs.Text = "Repairs view...";
            this.menuItemRepairs.Click += new System.EventHandler(this.menuItemRepairs_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 7;
            this.menuItem2.Text = "-";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 8;
            this.menuItem1.MergeOrder = 4;
            this.menuItem1.Text = "Close All";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItemClose
            // 
            this.menuItemClose.Index = 9;
            this.menuItemClose.MergeOrder = 5;
            this.menuItemClose.Text = "Cl&ose";
            this.menuItemClose.Click += new System.EventHandler(this.menuItemClose_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 10;
            this.menuItem3.Text = "-";
            // 
            // menuItemExit
            // 
            this.menuItemExit.Index = 11;
            this.menuItemExit.Text = "E&xit";
            this.menuItemExit.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuItemWindow
            // 
            this.menuItemWindow.Index = 1;
            this.menuItemWindow.MdiList = true;
            this.menuItemWindow.Text = "&Window";
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 2;
            this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemAI,
            this.menuItemAcs,
            this.menuItemHoriz,
            this.menuItemVert,
            this.menuItemMax,
            this.menuItemMin});
            this.menuItem5.Text = "&Layout";
            // 
            // menuItemAI
            // 
            this.menuItemAI.Index = 0;
            this.menuItemAI.Text = "Arrange&Icons";
            this.menuItemAI.Click += new System.EventHandler(this.menuItemAI_Click);
            // 
            // menuItemAcs
            // 
            this.menuItemAcs.Index = 1;
            this.menuItemAcs.Text = "&Cascade";
            this.menuItemAcs.Click += new System.EventHandler(this.menuItemCas_Click);
            // 
            // menuItemHoriz
            // 
            this.menuItemHoriz.Index = 2;
            this.menuItemHoriz.Text = "Arrange &Horizontal";
            this.menuItemHoriz.Click += new System.EventHandler(this.menuItemHoriz_Click);
            // 
            // menuItemVert
            // 
            this.menuItemVert.Index = 3;
            this.menuItemVert.Text = "Arrange &Vertical";
            this.menuItemVert.Click += new System.EventHandler(this.menuItemVert_Click);
            // 
            // menuItemMax
            // 
            this.menuItemMax.Index = 4;
            this.menuItemMax.Text = "Ma&ximize all";
            this.menuItemMax.Click += new System.EventHandler(this.menuItemMax_Click);
            // 
            // menuItemMin
            // 
            this.menuItemMin.Index = 5;
            this.menuItemMin.Text = "Mi&nimize all";
            this.menuItemMin.Click += new System.EventHandler(this.menuItemMin_Click);
            // 
            // oFileDlg
            // 
            this.oFileDlg.AddExtension = false;
            this.oFileDlg.Title = "MDI Sample";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 506);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(833, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(833, 528);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Menu = this.mainMenu1;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "My Super Fleet";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        private void menuItemClose_Click(object sender, System.EventArgs e)
		{
			Form a = this.ActiveMdiChild;
            if (a != null) {
                a.Close();
            }
		}		

		private void menuItemAI_Click(object sender, System.EventArgs e)
		{
			this.LayoutMdi(System.Windows.Forms.MdiLayout.ArrangeIcons);
		}

		private void menuItemCas_Click(object sender, System.EventArgs e)
		{
			this.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade);
		}

		private void menuItemHoriz_Click(object sender, System.EventArgs e)
		{
			this.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal);
		}

		private void menuItemVert_Click(object sender, System.EventArgs e)
		{			
			this.LayoutMdi(System.Windows.Forms.MdiLayout.TileVertical);
		}

		private void menuItemMax_Click(object sender, System.EventArgs e)
		{
			Form [] charr= this.MdiChildren;

			foreach (Form chform in charr)
				chform.WindowState=FormWindowState.Maximized;
		}

		private void menuItemMin_Click(object sender, System.EventArgs e)
		{
			Form [] charr= this.MdiChildren;

			foreach (Form chform in charr)
				chform.WindowState=FormWindowState.Minimized;
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			Form [] charr= this.MdiChildren;

			foreach (Form chform in charr)
				chform.Close();
		}

        private IContainer components;
        private MenuItem menuItemExit;

        private void menuItem2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private MenuItem menuItem3;
        private StatusStrip statusStrip1;
        private MenuItem menuItemFleet;
        private MenuItem menuItemVehicles;
        private MenuItem menuItemInspections;
        private MenuItem menuItemRepairs;
        private MenuItem menuItem2;
        private MenuItem menuItemVehiclesAndRepairs;
        private MenuItem menuItem4;

        private void menuItemFleet_Click(object sender, EventArgs e)
        {
            frmFleetsView chForm = new frmFleetsView();
            chForm.MdiParent = this;
            count++;
            chForm.Text = chForm.Text + " - " + count.ToString();
            chForm.Show();
        }

        private void menuItemVehicles_Click(object sender, EventArgs e)
        {
            frmVehiclesView chForm = new frmVehiclesView();
            chForm.MdiParent = this;
            count++;
            chForm.Text = chForm.Text + " - " + count.ToString();
            chForm.Show();
        }

        private void menuItemInspections_Click(object sender, EventArgs e)
        {
            frmInspectionsView chForm = new frmInspectionsView();
            chForm.MdiParent = this;
            count++;
            chForm.Text = chForm.Text + " - " + count.ToString();
            chForm.Show();
        }

        private void menuItemRepairs_Click(object sender, EventArgs e)
        {
            frmRepairsView chForm = new frmRepairsView();
            chForm.MdiParent = this;
            count++;
            chForm.Text = chForm.Text + " - " + count.ToString();
            chForm.Show();
        }


        private void menuItemVehiclesAndRepairs_Click(object sender, EventArgs e)
        {
            frmVehicles_RepairsView chForm = new frmVehicles_RepairsView();
            chForm.MdiParent = this;
            count++;
            chForm.Text = chForm.Text + " - " + count.ToString();
            chForm.Show();

        }

        private MenuItem menuItemReports;

        private void menuItemReports_Click(object sender, EventArgs e)
        {
            frmReportsView chForm = new frmReportsView();
            chForm.MdiParent = this;
            count++;
            chForm.Text = chForm.Text + " - " + count.ToString();
            chForm.Show();
        }
    }
}

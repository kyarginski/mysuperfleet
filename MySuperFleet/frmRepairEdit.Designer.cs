﻿namespace MySuperFleet
{
    partial class frmRepairEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.edNote = new System.Windows.Forms.TextBox();
            this.edRep_Date = new System.Windows.Forms.DateTimePicker();
            this.labelRep_Date = new System.Windows.Forms.Label();
            this.edRep_Num = new System.Windows.Forms.TextBox();
            this.labelRep_Num = new System.Windows.Forms.Label();
            this.panelButtom.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelButtom
            // 
            this.panelButtom.Size = new System.Drawing.Size(450, 178);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(369, 151);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(288, 151);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.groupBox1);
            this.panelMain.Controls.Add(this.edRep_Date);
            this.panelMain.Controls.Add(this.labelRep_Date);
            this.panelMain.Controls.Add(this.edRep_Num);
            this.panelMain.Controls.Add(this.labelRep_Num);
            this.panelMain.Size = new System.Drawing.Size(450, 256);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.edNote);
            this.groupBox1.Location = new System.Drawing.Point(16, 91);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 148);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Note";
            // 
            // edNote
            // 
            this.edNote.Location = new System.Drawing.Point(6, 19);
            this.edNote.Multiline = true;
            this.edNote.Name = "edNote";
            this.edNote.Size = new System.Drawing.Size(402, 123);
            this.edNote.TabIndex = 0;
            // 
            // edRep_Date
            // 
            this.edRep_Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.edRep_Date.Location = new System.Drawing.Point(290, 62);
            this.edRep_Date.Name = "edRep_Date";
            this.edRep_Date.Size = new System.Drawing.Size(140, 20);
            this.edRep_Date.TabIndex = 19;
            // 
            // labelRep_Date
            // 
            this.labelRep_Date.AutoSize = true;
            this.labelRep_Date.Location = new System.Drawing.Point(19, 68);
            this.labelRep_Date.Name = "labelRep_Date";
            this.labelRep_Date.Size = new System.Drawing.Size(64, 13);
            this.labelRep_Date.TabIndex = 18;
            this.labelRep_Date.Text = "Repair Date";
            // 
            // edRep_Num
            // 
            this.edRep_Num.Location = new System.Drawing.Point(290, 25);
            this.edRep_Num.Name = "edRep_Num";
            this.edRep_Num.Size = new System.Drawing.Size(140, 20);
            this.edRep_Num.TabIndex = 17;
            // 
            // labelRep_Num
            // 
            this.labelRep_Num.AutoSize = true;
            this.labelRep_Num.Location = new System.Drawing.Point(19, 28);
            this.labelRep_Num.Name = "labelRep_Num";
            this.labelRep_Num.Size = new System.Drawing.Size(78, 13);
            this.labelRep_Num.TabIndex = 16;
            this.labelRep_Num.Text = "Repair Number";
            // 
            // frmRepairEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 310);
            this.Name = "frmRepairEdit";
            this.Text = "Repair";
            this.panelButtom.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox edNote;
        public System.Windows.Forms.DateTimePicker edRep_Date;
        public System.Windows.Forms.Label labelRep_Date;
        public System.Windows.Forms.TextBox edRep_Num;
        public System.Windows.Forms.Label labelRep_Num;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySuperFleet
{
    public partial class frmVehicles_RepairsView : frmBaseView
    {
        public frmVehicles_RepairsView()
        {
            InitializeComponent();

            TableName = "VEHICLE_REP_LINK";
            IdName = "ID";

            doRefreshAction();

        }

        private String getSQLComboBoxValue(ComboBox comboBox) {
            String ret;

            if (comboBox.SelectedValue == null)
            {
                ret = "null";
            }
            else {
                ret = comboBox.SelectedValue.ToString();
            }

            return ret;
        }

        private void setSQLComboBoxValue(ComboBox comboBox, String value)
        {
            int id = 0;
            if (int.TryParse(value, out id))
            {
                comboBox.SelectedValue = id;
            }
            else {
                comboBox.SelectedIndex = -1;
            }

        }

        protected override void doRefreshAction()
        {
            dataGridView1.DataSource = Dm.openQuery("select * from V_VEHICLE_REP_LINK");
        }

        protected override void doAddAction()
        {

            frmVehicle_RepairEdit form = null;
            try
            {
                form = new frmVehicle_RepairEdit();
                form.Owner = this;
                form.Text = "Vehicle && Repair: Add";

                form.comboBoxParent.DataSource = Dm.openQuery("select ID, VEHICLE_CODE ||' '|| REPAIR_CODE as NAME from V_VEHICLE_REP_LINK");
                form.comboBoxParent.DisplayMember = "NAME";
                form.comboBoxParent.ValueMember = "ID";

                form.comboBoxRepair.DataSource = Dm.openQuery("select ID, REP_NUM ||' '|| REP_DATE as NAME  from REPAIRS");
                form.comboBoxRepair.DisplayMember = "NAME";
                form.comboBoxRepair.ValueMember = "ID";

                form.comboBoxVehicles.DataSource = Dm.openQuery("select ID, MODEL ||' '|| REG_NUM as NAME  from VEHICLES");
                form.comboBoxVehicles.DisplayMember = "NAME";
                form.comboBoxVehicles.ValueMember = "ID";

                form.comboBoxInspection.DataSource = Dm.openQuery("select ID, INS_NUM ||' '|| INS_DATE as NAME from INSPECTIONS");
                form.comboBoxInspection.DisplayMember = "NAME";
                form.comboBoxInspection.ValueMember = "ID";


                form.comboBoxParent.SelectedIndex = -1;
                form.comboBoxRepair.SelectedIndex = -1;
                form.comboBoxVehicles.SelectedIndex = -1;
                form.comboBoxInspection.SelectedIndex = -1;

                if (form.ShowDialog() == DialogResult.OK)
                {

                    Dm.executeQuery("insert into " + TableName + " (PARENT_ID,VEHICLE_ID,REPAIR_ID,INSPECTION_ID,START_DATE,SUCCESS) VALUES (" +
                              getSQLComboBoxValue(form.comboBoxParent) + "," +
                              getSQLComboBoxValue(form.comboBoxVehicles) + "," +
                              getSQLComboBoxValue(form.comboBoxRepair) + "," +
                              getSQLComboBoxValue(form.comboBoxInspection) + "," +
                        "'" + form.edStart_Date.Value.ToString("yyyy-MM-dd") + "'," +
                          (form.checkBoxSuccess.Checked ? "1" : "0") +
                        ")");
                    String idValue = Dm.getLastInsertedId(TableName).ToString();

                    setPositionById(idValue);

                }

            }
            finally
            {
                if (form != null)
                {
                    form.Close();
                }
            }
        }

        protected override void doEditAction()
        {
            int index = dataGridView1.SelectedRows[0].Index;

            if (index >= 0)
            {

                frmVehicle_RepairEdit form = null;
                try
                {
                    form = new frmVehicle_RepairEdit();
                    form.Owner = this;
                    form.Text = "Vehicle && Repair: Edit";

                    String idValue = this.dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[IdName].Value.ToString();

                    form.comboBoxParent.DataSource = Dm.openQuery("select ID, VEHICLE_CODE ||' '|| REPAIR_CODE as NAME from V_VEHICLE_REP_LINK where "+ IdName +" != "+ idValue);
                    form.comboBoxParent.DisplayMember = "NAME";
                    form.comboBoxParent.ValueMember = "ID";

                    form.comboBoxRepair.DataSource = Dm.openQuery("select ID, REP_NUM ||' '|| REP_DATE as NAME  from REPAIRS");
                    form.comboBoxRepair.DisplayMember = "NAME";
                    form.comboBoxRepair.ValueMember = "ID";

                    form.comboBoxVehicles.DataSource = Dm.openQuery("select ID, MODEL ||' '|| REG_NUM as NAME  from VEHICLES");
                    form.comboBoxVehicles.DisplayMember = "NAME";
                    form.comboBoxVehicles.ValueMember = "ID";

                    form.comboBoxInspection.DataSource = Dm.openQuery("select ID, INS_NUM ||' '|| INS_DATE as NAME from INSPECTIONS");
                    form.comboBoxInspection.DisplayMember = "NAME";
                    form.comboBoxInspection.ValueMember = "ID";

                    DataGridViewRow row = this.dataGridView1.Rows[index];

                    setSQLComboBoxValue(form.comboBoxParent, row.Cells["PARENT_ID"].Value.ToString());
                    setSQLComboBoxValue(form.comboBoxVehicles, row.Cells["VEHICLE_ID"].Value.ToString());
                    setSQLComboBoxValue(form.comboBoxRepair, row.Cells["REPAIR_ID"].Value.ToString());
                    setSQLComboBoxValue(form.comboBoxInspection, row.Cells["INSPECTION_ID"].Value.ToString());

                    form.checkBoxSuccess.Checked = Convert.ToBoolean(row.Cells["SUCCESS"].Value);

                    DateTime parsedDate;

                    if (DateTime.TryParseExact(row.Cells["START_DATE"].Value.ToString(), "yyyy-MM-dd", null,
                                              DateTimeStyles.None, out parsedDate))
                    {
                        form.edStart_Date.Value = parsedDate;
                    }


                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        //MessageBox.Show("Saved!", "My Super Fleet", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        Dm.executeQuery("update " + TableName + " set " +
                            "START_DATE = '" + form.edStart_Date.Value.ToString("yyyy-MM-dd") + "'," +
                            "PARENT_ID = " + getSQLComboBoxValue(form.comboBoxParent) + "," +
                            "VEHICLE_ID = " + getSQLComboBoxValue(form.comboBoxVehicles) + "," +
                            "REPAIR_ID = " + getSQLComboBoxValue(form.comboBoxRepair) + "," +
                            "INSPECTION_ID = " + getSQLComboBoxValue(form.comboBoxInspection) + "," +
                            "SUCCESS = " + (form.checkBoxSuccess.Checked ? "1" : "0") + 
                            " where " + IdName + "=" + idValue);

                        setPositionById(idValue);

                    }

                }
                finally
                {
                    if (form != null)
                    {
                        form.Close();
                    }
                }

            }

        }

    }
}

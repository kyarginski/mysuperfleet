﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySuperFleet
{
    public partial class frmRepairsView : frmBaseView
    {
        public frmRepairsView()
        {
            InitializeComponent();

            TableName = "REPAIRS";
            IdName = "ID";

            doRefreshAction();
        }

        protected override void doRefreshAction()
        {
            dataGridView1.DataSource = Dm.openQuery("select * from " + TableName);
        }

        protected override void doAddAction()
        {

            frmRepairEdit form = null;
            try
            {
                form = new frmRepairEdit();
                form.Owner = this;
                form.Text = "Repair: Add";


                if (form.ShowDialog() == DialogResult.OK)
                {
                    //MessageBox.Show("Saved!", "My Super Fleet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    /*
                      ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,
                      TYPE VARCHAR(50) NOT NULL,
                      INS_NUM VARCHAR(50) NOT NULL,
                      INS_DATE VARCHAR(10) NOT NULL,
                      NOTE TEXT
                    */


                    Dm.executeQuery("insert into " + TableName + " (REP_NUM,REP_DATE,NOTE) VALUES (" +
                        "'" + form.edRep_Num.Text + "'," +
                        "'" + form.edRep_Date.Value.ToString("yyyy-MM-dd") + "'," +
                        "'" + form.edNote.Text + "'" +
                        ")");
                    String idValue = Dm.getLastInsertedId(TableName).ToString();

                    setPositionById(idValue);

                }

            }
            finally
            {
                if (form != null)
                {
                    form.Close();
                }
            }
        }

        protected override void doEditAction()
        {
            int index = dataGridView1.SelectedRows[0].Index;

            if (index >= 0)
            {

                frmRepairEdit form = null;
                try
                {
                    form = new frmRepairEdit();
                    form.Owner = this;
                    form.Text = "Repair: Edit";


                    DataGridViewRow row = this.dataGridView1.Rows[index];

                    form.edRep_Num.Text = row.Cells["REP_NUM"].Value.ToString();

                    DateTime parsedDate;

                    if (DateTime.TryParseExact(row.Cells["REP_DATE"].Value.ToString(), "yyyy-MM-dd", null,
                                              DateTimeStyles.None, out parsedDate))
                    {
                        form.edRep_Date.Value = parsedDate;
                    }


                    form.edNote.Text = row.Cells["NOTE"].Value.ToString();

                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        //MessageBox.Show("Saved!", "My Super Fleet", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        String idValue = this.dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[IdName].Value.ToString();

                        Dm.executeQuery("update " + TableName + " set " +
                            "REP_NUM = '" + form.edRep_Num.Text + "'," +
                            "REP_DATE = '" + form.edRep_Date.Value.ToString("yyyy-MM-dd") + "'," +
                            "NOTE = '" + form.edNote.Text + "'" +
                            " where " + IdName + "=" + idValue);

                        setPositionById(idValue);

                    }

                }
                finally
                {
                    if (form != null)
                    {
                        form.Close();
                    }
                }

            }

        }


    }
}

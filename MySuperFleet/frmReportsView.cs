﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySuperFleet
{
    public partial class frmReportsView : frmBaseView
    {
        public System.Windows.Forms.ToolStripButton btnRun;

        public frmReportsView()
        {
            InitializeComponent();

            newRunButton();

            TableName = "REPORTS";
            IdName = "ID";

            doRefreshAction();
        }

        private void newRunButton() {
            btnRun = new System.Windows.Forms.ToolStripButton();

            this.btnRun.AutoSize = false;
            this.btnRun.Image = global::MySuperFleet.Properties.Resources.report;
            this.btnRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(50, 50);
            this.btnRun.Text = "Run";
            this.btnRun.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnRun.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnRun.ToolTipText = "Run report";
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                this.btnRun
            });
            this.btnRun.Visible = true;

        }




        private void btnRun_Click(object sender, EventArgs e)
        {
            Makereport();
        }

        private void Makereport()
        {

            int index = dataGridView1.SelectedRows[0].Index;

            if (index >= 0)
            {

                String filePath;
                String queryText;
                String reportType;
                DataGridViewRow row = this.dataGridView1.Rows[index];

                filePath = row.Cells["REPORT_NAME"].Value.ToString();
                queryText = row.Cells["SQL_TEXT"].Value.ToString();
                reportType = row.Cells["REPORT_TYPE"].Value.ToString();

                if (reportType.Equals("CSV"))
                {

                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.FileName = filePath;
                    saveFileDialog1.Title = "Specify Destination Filename";
                    saveFileDialog1.Filter = "CSV-files|*.csv|All files|*.*";
                    saveFileDialog1.FilterIndex = 1;
                    saveFileDialog1.OverwritePrompt = true;
                    if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
                    {
                        filePath = saveFileDialog1.FileName;
                        DataTable reportData = Dm.openQuery(queryText);
                        Dm.WriteToCsvFile(reportData, filePath);
                        MessageBox.Show("Report was saved to " + filePath, "My Super Fleet", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
                else {
                    MessageBox.Show("Report type " + reportType + " not supported yet.", "My Super Fleet", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        protected override void doRefreshAction()
        {
            dataGridView1.DataSource = Dm.openQuery("select * from " + TableName);
        }

        protected override void doAddAction()
        {

            frmReportsEdit form = null;
            try
            {
                form = new frmReportsEdit();
                form.Owner = this;
                form.Text = "Report: Add";


                if (form.ShowDialog() == DialogResult.OK)
                {

                    Dm.executeQuery("insert into " + TableName + " (REPORT_NAME,REPORT_TYPE,SQL_TEXT) VALUES (" +
                        "'" + form.edReport_Name.Text + "'," +
                        "'" + form.comboReport_Type.Text + "'," +
                        "'" + form.edSQL_Text.Text + "'" +
                        ")");
                    String idValue = Dm.getLastInsertedId(TableName).ToString();

                    setPositionById(idValue);

                }

            }
            finally
            {
                if (form != null)
                {
                    form.Close();
                }
            }
        }

        protected override void doEditAction()
        {
            int index = dataGridView1.SelectedRows[0].Index;

            if (index >= 0)
            {

                frmReportsEdit form = null;
                try
                {
                    form = new frmReportsEdit();
                    form.Owner = this;
                    form.Text = "Report: Edit";


                    DataGridViewRow row = this.dataGridView1.Rows[index];

                    form.edReport_Name.Text = row.Cells["REPORT_NAME"].Value.ToString();
                    form.comboReport_Type.Text = row.Cells["REPORT_TYPE"].Value.ToString();
                    form.edSQL_Text.Text = row.Cells["SQL_TEXT"].Value.ToString();

                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        //MessageBox.Show("Saved!", "My Super Fleet", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        String idValue = this.dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[IdName].Value.ToString();

                        Dm.executeQuery("update " + TableName + " set " +
                            "REPORT_NAME = '" + form.edReport_Name.Text + "'," +
                            "REPORT_TYPE = '" + form.comboReport_Type.Text + "'," +
                            "SQL_TEXT = '" + form.edSQL_Text.Text + "'" +
                            " where " + IdName + "=" + idValue);

                        setPositionById(idValue);

                    }

                }
                finally
                {
                    if (form != null)
                    {
                        form.Close();
                    }
                }

            }

        }


    }
}

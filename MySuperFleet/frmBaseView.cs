﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Windows.Forms;

namespace MySuperFleet
{
    public partial class frmBaseView : Form
    {
        private DataModule dm;

        private String selectQuery;

        private String tableName;

        private String idName;


        public string SelectQuery
        {
            get
            {
                return selectQuery;
            }

            set
            {
                selectQuery = value;
            }
        }

        internal DataModule Dm
        {
            get
            {
                return dm;
            }

            set
            {
                dm = value;
            }
        }

        public string TableName
        {
            get
            {
                return tableName;
            }

            set
            {
                tableName = value;
            }
        }

        public string IdName
        {
            get
            {
                return idName;
            }

            set
            {
                idName = value;
            }
        }

        public frmBaseView()
        {
            InitializeComponent();

            dm = new DataModule();

            IdName = "ID";
        }


        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            doEditAction();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            doRefreshAction();
        }

        protected virtual void doRefreshAction()
        {
            //throw new NotImplementedException();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            doAddAction();
        }

        protected virtual void doAddAction()
        {
            //throw new NotImplementedException();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            doEditAction();
        }

        protected virtual void doEditAction()
        {
            //throw new NotImplementedException();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            doDeleteAction();
        }

        protected virtual void doDeleteAction()
        {
            String idValue = this.dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[idName].Value.ToString();

            if (MessageBox.Show("Delete selected row with "+ idName + " = "+ idValue + "?", "My Super Fleet", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                dm.executeQuery("delete from "+ tableName + " where "+idName+"="+ idValue);
                doRefreshAction();
            }
        }

        protected virtual void setPositionById(String id)
        {
            doRefreshAction();

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((row.Cells[0].Value != null) &&(row.Cells[0].Value.ToString().Equals(id)))
                {
                    dataGridView1.Rows[row.Index].Selected = true;
                    dataGridView1.CurrentCell = dataGridView1.Rows[row.Index].Cells[0];
                    break;
                }
            }

        }


    }
}

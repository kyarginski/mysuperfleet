﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySuperFleet
{
    public partial class frmFleetView : frmBaseView
    {
        public frmFleetView()
        {
            InitializeComponent();

            TableName = "FLEETS";
            IdName = "ID";

            doRefreshAction();

        }

        protected override void doRefreshAction()
        {
            dataGridView1.DataSource = Dm.openQuery("select ID, NAME from "+ TableName);
        }

        protected override void doEditAction()
        {
            int index = dataGridView1.SelectedRows[0].Index;

            if (index >= 0)
            {

                frmFleetEdit form = null;
                try
                {
                    form = new frmFleetEdit();
                    form.Owner = this;
                    form.Text = "Fleet: Edit";
                    DataGridViewRow row = this.dataGridView1.Rows[index];
                    form.edFleetName.Text = row.Cells["NAME"].Value.ToString();
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        //MessageBox.Show("Saved!", "My Super Fleet", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        String idValue = this.dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[IdName].Value.ToString();
                        String nameValue = form.edFleetName.Text;

                        Dm.executeQuery("update " + TableName + " set NAME = '"+ nameValue + "' where " + IdName + "=" + idValue);

                        doRefreshAction();
                        setPositionById(idValue);

                    }

                }
                finally
                {
                    if (form != null)
                    {
                        form.Close();
                    }
                }

            }

        }

    }
}

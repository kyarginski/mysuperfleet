﻿namespace MySuperFleet
{
    partial class frmVehicle_RepairEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelParent = new System.Windows.Forms.Label();
            this.comboBoxParent = new System.Windows.Forms.ComboBox();
            this.edStart_Date = new System.Windows.Forms.DateTimePicker();
            this.labelStart_Date = new System.Windows.Forms.Label();
            this.labelRepair = new System.Windows.Forms.Label();
            this.comboBoxRepair = new System.Windows.Forms.ComboBox();
            this.labelVehicle = new System.Windows.Forms.Label();
            this.comboBoxVehicles = new System.Windows.Forms.ComboBox();
            this.checkBoxSuccess = new System.Windows.Forms.CheckBox();
            this.labelInspection = new System.Windows.Forms.Label();
            this.comboBoxInspection = new System.Windows.Forms.ComboBox();
            this.panelButtom.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelButtom
            // 
            this.panelButtom.Controls.Add(this.checkBoxSuccess);
            this.panelButtom.Location = new System.Drawing.Point(12, 184);
            this.panelButtom.Size = new System.Drawing.Size(501, 69);
            this.panelButtom.Controls.SetChildIndex(this.buttonOK, 0);
            this.panelButtom.Controls.SetChildIndex(this.buttonCancel, 0);
            this.panelButtom.Controls.SetChildIndex(this.checkBoxSuccess, 0);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(420, 42);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(339, 42);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.labelInspection);
            this.panelMain.Controls.Add(this.comboBoxInspection);
            this.panelMain.Controls.Add(this.labelVehicle);
            this.panelMain.Controls.Add(this.comboBoxVehicles);
            this.panelMain.Controls.Add(this.labelRepair);
            this.panelMain.Controls.Add(this.comboBoxRepair);
            this.panelMain.Controls.Add(this.labelParent);
            this.panelMain.Controls.Add(this.comboBoxParent);
            this.panelMain.Controls.Add(this.edStart_Date);
            this.panelMain.Controls.Add(this.labelStart_Date);
            this.panelMain.Size = new System.Drawing.Size(501, 208);
            // 
            // labelParent
            // 
            this.labelParent.AutoSize = true;
            this.labelParent.Location = new System.Drawing.Point(25, 55);
            this.labelParent.Name = "labelParent";
            this.labelParent.Size = new System.Drawing.Size(128, 13);
            this.labelParent.TabIndex = 5;
            this.labelParent.Text = "Previous Repair (optional)";
            // 
            // comboBoxParent
            // 
            this.comboBoxParent.FormattingEnabled = true;
            this.comboBoxParent.Location = new System.Drawing.Point(173, 52);
            this.comboBoxParent.Name = "comboBoxParent";
            this.comboBoxParent.Size = new System.Drawing.Size(307, 21);
            this.comboBoxParent.TabIndex = 6;
            // 
            // edStart_Date
            // 
            this.edStart_Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.edStart_Date.Location = new System.Drawing.Point(339, 172);
            this.edStart_Date.Name = "edStart_Date";
            this.edStart_Date.Size = new System.Drawing.Size(140, 20);
            this.edStart_Date.TabIndex = 12;
            // 
            // labelStart_Date
            // 
            this.labelStart_Date.AutoSize = true;
            this.labelStart_Date.Location = new System.Drawing.Point(246, 178);
            this.labelStart_Date.Name = "labelStart_Date";
            this.labelStart_Date.Size = new System.Drawing.Size(55, 13);
            this.labelStart_Date.TabIndex = 11;
            this.labelStart_Date.Text = "Start Date";
            // 
            // labelRepair
            // 
            this.labelRepair.AutoSize = true;
            this.labelRepair.Location = new System.Drawing.Point(25, 19);
            this.labelRepair.Name = "labelRepair";
            this.labelRepair.Size = new System.Drawing.Size(75, 13);
            this.labelRepair.TabIndex = 3;
            this.labelRepair.Text = "Current Repair";
            // 
            // comboBoxRepair
            // 
            this.comboBoxRepair.FormattingEnabled = true;
            this.comboBoxRepair.Location = new System.Drawing.Point(173, 16);
            this.comboBoxRepair.Name = "comboBoxRepair";
            this.comboBoxRepair.Size = new System.Drawing.Size(307, 21);
            this.comboBoxRepair.TabIndex = 4;
            // 
            // labelVehicle
            // 
            this.labelVehicle.AutoSize = true;
            this.labelVehicle.Location = new System.Drawing.Point(25, 94);
            this.labelVehicle.Name = "labelVehicle";
            this.labelVehicle.Size = new System.Drawing.Size(42, 13);
            this.labelVehicle.TabIndex = 7;
            this.labelVehicle.Text = "Vehicle";
            // 
            // comboBoxVehicles
            // 
            this.comboBoxVehicles.FormattingEnabled = true;
            this.comboBoxVehicles.Location = new System.Drawing.Point(173, 91);
            this.comboBoxVehicles.Name = "comboBoxVehicles";
            this.comboBoxVehicles.Size = new System.Drawing.Size(307, 21);
            this.comboBoxVehicles.TabIndex = 8;
            // 
            // checkBoxSuccess
            // 
            this.checkBoxSuccess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxSuccess.AutoSize = true;
            this.checkBoxSuccess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxSuccess.Location = new System.Drawing.Point(28, 43);
            this.checkBoxSuccess.Name = "checkBoxSuccess";
            this.checkBoxSuccess.Size = new System.Drawing.Size(74, 17);
            this.checkBoxSuccess.TabIndex = 20;
            this.checkBoxSuccess.Text = "Success";
            this.checkBoxSuccess.UseVisualStyleBackColor = true;
            // 
            // labelInsprction
            // 
            this.labelInspection.AutoSize = true;
            this.labelInspection.Location = new System.Drawing.Point(25, 134);
            this.labelInspection.Name = "labelInsprction";
            this.labelInspection.Size = new System.Drawing.Size(56, 13);
            this.labelInspection.TabIndex = 9;
            this.labelInspection.Text = "Inspection";
            // 
            // comboBoxInspection
            // 
            this.comboBoxInspection.FormattingEnabled = true;
            this.comboBoxInspection.Location = new System.Drawing.Point(173, 131);
            this.comboBoxInspection.Name = "comboBoxInspection";
            this.comboBoxInspection.Size = new System.Drawing.Size(307, 21);
            this.comboBoxInspection.TabIndex = 10;
            // 
            // frmVehicle_RepairEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 262);
            this.Name = "frmVehicle_RepairEdit";
            this.Text = "frmVehicle_RepairEdit";
            this.panelButtom.ResumeLayout(false);
            this.panelButtom.PerformLayout();
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.CheckBox checkBoxSuccess;
        public System.Windows.Forms.Label labelVehicle;
        public System.Windows.Forms.ComboBox comboBoxVehicles;
        public System.Windows.Forms.Label labelRepair;
        public System.Windows.Forms.ComboBox comboBoxRepair;
        public System.Windows.Forms.Label labelParent;
        public System.Windows.Forms.ComboBox comboBoxParent;
        public System.Windows.Forms.DateTimePicker edStart_Date;
        public System.Windows.Forms.Label labelStart_Date;
        public System.Windows.Forms.Label labelInspection;
        public System.Windows.Forms.ComboBox comboBoxInspection;
    }
}
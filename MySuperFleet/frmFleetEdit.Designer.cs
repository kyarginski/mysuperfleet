﻿using System.Windows.Forms;

namespace MySuperFleet
{
    partial class frmFleetEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelFleetName = new System.Windows.Forms.Label();
            this.edFleetName = new System.Windows.Forms.TextBox();
            this.panelButtom.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelButtom
            // 
            this.panelButtom.Size = new System.Drawing.Size(374, 30);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(293, 3);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(212, 3);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.edFleetName);
            this.panelMain.Controls.Add(this.labelFleetName);
            this.panelMain.Size = new System.Drawing.Size(374, 108);
            // 
            // labelFleetName
            // 
            this.labelFleetName.AutoSize = true;
            this.labelFleetName.Location = new System.Drawing.Point(43, 46);
            this.labelFleetName.Name = "labelFleetName";
            this.labelFleetName.Size = new System.Drawing.Size(61, 13);
            this.labelFleetName.TabIndex = 0;
            this.labelFleetName.Text = "Fleet Name";
            // 
            // textBox1
            // 
            this.edFleetName.Location = new System.Drawing.Point(121, 43);
            this.edFleetName.Name = "textBox1";
            this.edFleetName.Size = new System.Drawing.Size(228, 20);
            this.edFleetName.TabIndex = 1;
            // 
            // frmFleetEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 162);
            this.Name = "frmFleetEdit";
            this.Text = "Fleet";
            this.panelButtom.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelFleetName;
        public System.Windows.Forms.TextBox edFleetName;
    }
}
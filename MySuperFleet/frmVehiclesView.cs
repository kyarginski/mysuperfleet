﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySuperFleet
{
    public partial class frmVehiclesView : frmBaseView
    {
        public frmVehiclesView()
        {
            InitializeComponent();

            TableName = "VEHICLES";
            IdName = "ID";

            doRefreshAction();

        }
        protected override void doRefreshAction()
        {
            dataGridView1.DataSource = Dm.openQuery("select * from V_VEHICLES");
        }

        protected override void doAddAction()
        {

            frmVehicleEdit form = null;
            try
            {
                form = new frmVehicleEdit();
                form.Owner = this;
                form.Text = "Vehicle: Add";

                form.comboBoxFleet.DataSource = Dm.openQuery("select ID, NAME from FLEETS");
                form.comboBoxFleet.DisplayMember = "NAME";
                form.comboBoxFleet.ValueMember = "ID";


                if (form.ShowDialog() == DialogResult.OK)
                {
                    //MessageBox.Show("Saved!", "My Super Fleet", MessageBoxButtons.OK, MessageBoxIcon.Information);
/*
                    TYPE VARCHAR(50) NOT NULL,
                    MODEL VARCHAR(250) NOT NULL,
                    REG_NUM VARCHAR(50) NOT NULL,
                    REG_DATE VARCHAR(10) NOT NULL,
                    FLEET_ID INTEGER,
                    NOTE VARCHAR(250),
*/


                    Dm.executeQuery("insert into " + TableName + " (TYPE,MODEL,REG_NUM,REG_DATE,FLEET_ID,NOTE) VALUES (" + 
                        "'" + form.edType.Text + "'," +
                        "'" + form.edModel.Text + "'," +
                        "'" + form.edReg_Num.Text + "'," +
                        "'" + form.edReg_Date.Value.ToString("yyyy-MM-dd") + "'," +
                              form.comboBoxFleet.SelectedValue + "," +
                        "'" + form.edNote.Text + "'" +
                        ")");
                    String idValue = Dm.getLastInsertedId(TableName).ToString();

                    setPositionById(idValue);

                }

            }
            finally
            {
                if (form != null)
                {
                    form.Close();
                }
            }
        }

        protected override void doEditAction()
        {
            int index = dataGridView1.SelectedRows[0].Index;

            if (index >= 0)
            {

                frmVehicleEdit form = null;
                try
                {
                    form = new frmVehicleEdit();
                    form.Owner = this;
                    form.Text = "Vehicle: Edit";

                    form.comboBoxFleet.DataSource = Dm.openQuery("select ID, NAME from FLEETS");
                    form.comboBoxFleet.DisplayMember = "NAME";
                    form.comboBoxFleet.ValueMember = "ID";

                    DataGridViewRow row = this.dataGridView1.Rows[index];

                    form.edType.Text = row.Cells["TYPE"].Value.ToString();
                    form.edModel.Text = row.Cells["MODEL"].Value.ToString();
                    form.edReg_Num.Text = row.Cells["REG_NUM"].Value.ToString();

                    DateTime parsedDate;

                    if (DateTime.TryParseExact(row.Cells["REG_DATE"].Value.ToString(), "yyyy-MM-dd", null,
                                              DateTimeStyles.None, out parsedDate))
                    {
                        form.edReg_Date.Value = parsedDate;
                    }


                    form.comboBoxFleet.SelectedValue = int.Parse(row.Cells["FLEET_ID"].Value.ToString());
                    form.edNote.Text = row.Cells["NOTE"].Value.ToString();

                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        //MessageBox.Show("Saved!", "My Super Fleet", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        String idValue = this.dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[IdName].Value.ToString();

                        Dm.executeQuery("update " + TableName + " set " +
                            "TYPE = '" + form.edType.Text + "'," +
                            "MODEL = '" + form.edModel.Text + "'," +
                            "REG_NUM = '" + form.edReg_Num.Text + "'," +
                            "REG_DATE = '" + form.edReg_Date.Value.ToString("yyyy-MM-dd") + "'," +
                            "FLEET_ID = " + form.comboBoxFleet.SelectedValue + "," +
                            "NOTE = '" + form.edNote.Text + "'" +
                            " where " + IdName + "=" + idValue);


                        setPositionById(idValue);

                    }

                }
                finally
                {
                    if (form != null)
                    {
                        form.Close();
                    }
                }

            }

        }

    }

}

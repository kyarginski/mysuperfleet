﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySuperFleet
{
    public partial class frmBaseEdit : Form
    {
        public frmBaseEdit()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        protected virtual bool doValidate()
        {
           return true;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.None;
            if (doValidate()) {
                this.DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}

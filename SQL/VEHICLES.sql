DROP TABLE IF EXISTS VEHICLES;

DROP INDEX IF EXISTS VEHICLES_ID_uindex;

CREATE TABLE IF NOT EXISTS VEHICLES
(
  ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,
  TYPE VARCHAR(50) NOT NULL,
  MODEL VARCHAR(250) NOT NULL,
  REG_NUM VARCHAR(50) NOT NULL,
  REG_DATE VARCHAR(10) NOT NULL,
  FLEET_ID INTEGER ,
  NOTE VARCHAR(250),
  FOREIGN KEY(FLEET_ID) REFERENCES FLEET(ID)
);

CREATE UNIQUE INDEX VEHICLES_ID_uindex ON VEHICLES (ID);
